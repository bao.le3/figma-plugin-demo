import * as React from "react";
import { useRef, useEffect } from "react";
import { Select, Input, Checkbox } from "figma-styled-components";

import { useGoogleMapContext } from "../../hooks/useGoogleMap";
import { Line } from "../Line";
import { Label } from "../Label";
import { t } from "i18next";
import { MAP_TYPES } from "../../constants/map";

const GoogleMapInputs = () => {
  const [store, dispatch] = useGoogleMapContext();

  const input = useRef<HTMLInputElement>(null);
  useEffect(() => {
    if (input.current) {
      input.current.focus();
    }
  }, []);

  return (
    <div>
      <div>
        <Label label={t('address')}></Label>
        <div style={{ padding: "0 8px" }}>
          <input
            ref={input}
            className="input"
            placeholder={t('input_address_here')}
            value={store.options.address}
            onInput={(e: any) =>
              dispatch({ type: "INPUT_ADDRESS", value: e.target.value })
            }
          />
        </div>
      </div>
      <Line />
      <div>
        <Label label={t('map_type')}></Label>
        <div style={{ padding: "4px 8px 0" }}>
          <Select
            onChange={({ value }: { value: string }) => {
              if (
                "roadmap" === value ||
                "satellite" === value ||
                "hybrid" === value ||
                "terrain" === value
              ) {
                dispatch({
                  type: "INPUT_MAP_TYPE",
                  value
                });
              }
            }}
            value={store.options.type}
            options={MAP_TYPES}
          ></Select>
        </div>
      </div>
      <Line />
      <div>
        <Label label={t('zoom_level')}></Label>
        <div style={{ padding: "4px 8px 0" }}>
          <Input
            type="number"
            onChange={(e: any) => {
              const val = e.target.value;
              if (val !== "") {
                dispatch({
                  type: "INPUT_ZOOM",
                  value: Number(e.target.value)
                });
              } else {
                dispatch({
                  type: "INPUT_ZOOM",
                  value: ""
                });
              }
            }}
            value={store.options.zoom}
          />
        </div>
      </div>
      <div style={{ padding: "0 6px" }}>
        <Checkbox
          checked={store.options.marker}
          label={t('show_marker')}
          onChange={(e: any) => {
            console.log(e.target.checked);
            dispatch({
              type: "INPUT_MARKER",
              value: e.target.checked
            });
          }}
        />
      </div>
      <Line />
      <div>
        <Label label={t('custom_style')}></Label>
        <div style={{ padding: "4px 16px 0" }}>
          <textarea
            className="textarea"
            onInput={(e: any) =>
              dispatch({ type: "INPUT_JSON", value: e.target.value })
            }
            style={{ width: "100%", margin: 0 }}
            rows={5}
            placeholder={t('paste_your_section_here')}
          >
            {store.options.json}
          </textarea>
          {store.jsonIsInvalid && (
            <p className="type--12-pos" style={{ color: "#f24822" }}>
              {t('invalid_json')}
            </p>
          )}
          <p className="type--12-pos">
            {t('find_at_more_here')}:
            <a target="__blank" href="https://snazzymaps.com/explore">
              {t('snazzy_map')}
            </a>
            <a target="__blank" href="https://mapstyle.withgoogle.com/">
              {t('google_offical_map_style')}
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export { GoogleMapInputs };
