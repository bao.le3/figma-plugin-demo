import * as React from "react";
import { useRef, useEffect } from "react";
import { Select } from "figma-styled-components";
import { useMapboxContext } from "../../hooks/useMapbox";
import { Line } from "../Line";
import { Label } from "../Label";
import { MAP_STYLES } from "../../constants/map";
import { t } from "i18next";

const MapboxInputs = () => {
  const [store, dispatch] = useMapboxContext();

  const input = useRef<HTMLInputElement>(null);
  useEffect(() => {
    if (input.current !== null) {
      input.current.focus();
    }
  }, []);

  return (
    <div>
      <div>
        <Label label={t('address')}></Label>
        <div style={{ padding: "0 8px" }}>
          <input
            ref={input}
            className="input"
            placeholder={t('input_address_here')}
            value={store.options.address}
            onInput={(e: any) =>
              dispatch({ type: "INPUT_ADDRESS", value: e.target.value })
            }
          />
        </div>
      </div>
      <Line />
      <div>
        <Label label={t('style')}></Label>
        <div style={{ padding: "4px 8px 0" }}>
          <Select
            onChange={({ value }: { value: string }) => {
              if (
                value === "streets-v11" ||
                value === "light-v10" ||
                value === "dark-v10" ||
                value === "outdoors-v11" ||
                value === "satellite-v9" ||
                value === "satellite-streets-v11"
              ) {
                dispatch({
                  type: "INPUT_TYPE",
                  value
                });
              }
            }}
            value={store.options.type}
            options={MAP_STYLES}
          ></Select>
        </div>
      </div>
      <Line />
      <div>
        <Label label={t('zoom_level')}></Label>
        <div style={{ padding: "0 8px" }}>
          <input
            type="number"
            className="input"
            placeholder={t('zoom_level')}
            value={store.options.zoom}
            onInput={(e: any) =>
              dispatch({ type: "INPUT_ZOOM", value: e.target.value })
            }
          />
        </div>
      </div>
      <Line />
      <div>
        <Label label={t('bearing')}></Label>
        <div style={{ padding: "0 8px" }}>
          <div
            style={{
              padding: "0 8px",
              marginBottom: 4,
              color: "rgba(0, 0, 0, 0.3)"
            }}
            className="type--12-pos"
          >
            {t('rotates_the_map_around_its_center')}
          </div>
          <input
            type="number"
            className="input"
            placeholder={t('bearing')}
            value={store.options.bearing}
            onInput={(e: any) =>
              dispatch({ type: "INPUT_BEARING", value: e.target.value })
            }
          />
        </div>
      </div>
      <Line />
      <div>
        <Label label={t('pitch')}></Label>
        <div style={{ padding: "0 8px" }}>
          <div
            style={{
              padding: "0 8px",
              color: "rgba(0, 0, 0, 0.3)",
              marginBottom: 4
            }}
            className="type--12-pos"
          >
            {t('titles_the_map')}
          </div>
          <input
            type="number"
            className="input"
            placeholder={t('pitch')}
            value={store.options.pitch}
            onInput={(e: any) =>
              dispatch({ type: "INPUT_PITCH", value: e.target.value })
            }
          />
        </div>
      </div>
      <Line />
    </div>
  );
};

export { MapboxInputs };
