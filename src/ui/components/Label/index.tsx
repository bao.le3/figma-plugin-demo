import * as React from "react";
import "./style.scss"
const Label = ({ label }: { label: string }) => (
  <p className="type--11-pos-bold line">
    {label}
  </p>
);

export { Label };
