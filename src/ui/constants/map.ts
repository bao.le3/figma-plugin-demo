export const MAP_TYPES = [
    { label: "Roadmap", value: "roadmap" },
    { label: "Satellite", value: "satellite" },
    { label: "Hybrid", value: "hybrid" },
    { label: "Terrain", value: "terrain" }
];

export const MAP_STYLES = [
    { label: "Mapbox Streets", value: "streets-v11" },
    { label: "Mapbox Light", value: "light-v10" },
    { label: "Mapbox Dark", value: "dark-v10" },
    { label: "Mapbox Outdoors", value: "outdoors-v11" },
    { label: "Mapbox Satellite", value: "satellite-v9" },
    {
      label: "Mapbox Satellite Streets",
      value: "satellite-streets-v11"
    }
]